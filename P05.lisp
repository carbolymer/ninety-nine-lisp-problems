(reverse-list '(a b c d e))

(defun reverse-list (the-list)
  (if the-list
    (append (reverse-list (cdr the-list)) (list (car the-list)))
      nil))







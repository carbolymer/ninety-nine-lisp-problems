(count-elems '(a b c d e))
(count-elems '())
(count-elems '(a))

(defun count-elems (the-list)
  (if (cdr the-list)
      (+ 1 (count-elems (cdr the-list)))
      (if (car the-list) 1 0)))

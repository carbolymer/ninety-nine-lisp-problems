(encode '(a a a a b c c a a d e e e e))
(encode '(a a b))
(encode '(a b))
(encode '(a))
(encode '())


(defun encode (the-list)
  (run-length (encode_ (mapcar #'list the-list))))

(defun run-length (the-list)
  (mapcar (lambda (the-group) `(,(length the-group) ,(car the-group))) the-list))

(defun encode_ (the-list)
  (cond ((null the-list) nil)
        ((equal (car the-list) (cadr the-list))
         (let* ((encodeed (encode_ (cdr the-list)))
                (fst-encodeed (car encodeed))
                (tail-encodeed (cdr encodeed)))
           (cons (append (car the-list) fst-encodeed) tail-encodeed)))
        (t (cons (car the-list) (encode_ (cdr the-list))))))


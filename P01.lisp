(my-last '(a b c d))

(defun my-last (the-list)
  (if (cdr the-list)
      (my-last (cdr the-list))
      (car the-list)))

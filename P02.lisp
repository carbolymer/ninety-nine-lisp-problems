(my-but-last '(a b c d))

(defun my-but-last (the-list)
  (if (cdr (cdr the-list))
      (my-but-last (cdr the-list))
      (car the-list)))

(element-at '(a b c d e) 3)

; the first element is number 1
(defun element-at (the-list n)
  (if (<= n 1)
      (car the-list)
      (element-at (cdr the-list) (- n 1))))

(decode '((4 A) B (2 C) (2 A) D (4 E)))
(decode '((4 A)))
(decode '(A))
(decode '())

(decode-el (cdr '((4 A))))

(defun decode (the-list)
  (if (null the-list) 
      '()
      (append (decode-el (car the-list)) (decode (cdr the-list)))))

(defun decode-el (el)
  (cond ((null el) '())
        ((listp el) (repeat (car el) (cadr el)))
        (T `(,el))))

(defun repeat (n el)
  (if (> n 0)
      (cons el (repeat (- n 1) el))
      '()))

(compress '(a a a a b c c a a d e e e e))
(compress '(a a b))
(compress '(a b c))
(compress '())

(if (null nil) 'A 'B)

(defun compress (the-list)
  (if (null the-list)
      nil
      (if (equal (car (cdr the-list)) (car the-list))
          ; skip first element if it's equal to the second one
          (compress (cdr the-list))
          ; not equal, skip first element, cons car and processed cdr
          (cons (car the-list) (compress (cdr the-list))))))


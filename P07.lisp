(my-flatten '(a b c))
(my-flatten '(a (b (c d) e)))

(listp '(1))

(defmethod gcar ((l list)) (car l)) 
(defmethod gcar (l) nil)

(defun my-flatten (the-list)
  (if (gcar (gcar the-list)) ; check if first element is a list
      (append
        (my-flatten (gcar the-list))
        (my-flatten (cdr the-list)))
      (if (gcar the-list)
          (cons
            (car the-list)
            (my-flatten (cdr the-list)))
          nil)))


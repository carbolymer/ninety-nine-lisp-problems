(pack '(a a a a b c c a a d e e e e))
(pack '(a a b))
(pack '(a b))
(pack '(a))
(pack '())

(defun pack (the-list)
  (pack_ (mapcar #'list the-list)))

(defun pack_ (the-list)
  (cond ((null the-list) nil)
        ((equal (car the-list) (cadr the-list)) 
         (let* ((packed (pack_ (cdr the-list)))
                (fst-packed (car packed))
                (tail-packed (cdr packed)))
           (cons (append (car the-list) fst-packed) tail-packed)))
        (t (cons (car the-list) (pack_ (cdr the-list))))))


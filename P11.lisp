(encode-modified '(a a a a b c c a a d e e e e))
(encode-modified '(a a b))
(encode-modified '(a b))
(encode-modified '(a))
(encode-modified '())


(defun encode-modified (the-list)
  (mapcar #'run-length (encode-modified_ (mapcar #'list the-list))))

(defun run-length (the-group)
  (if (equal (length the-group) 1) (car the-group)
      `(,(length the-group) ,(car the-group))))

(defun encode-modified_ (the-list)
  (cond ((null the-list) nil)
        ((equal (car the-list) (cadr the-list))
         (let* ((encode-modifieded (encode-modified_ (cdr the-list)))
                (fst-encode-modifieded (car encode-modifieded))
                (tail-encode-modifieded (cdr encode-modifieded)))
           (cons (append (car the-list) fst-encode-modifieded) tail-encode-modifieded)))
        (t (cons (car the-list) (encode-modified_ (cdr the-list))))))


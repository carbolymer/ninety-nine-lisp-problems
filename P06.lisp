(is-palindrome '(x a m a x))
(is-palindrome '(x a m a y))

(defun is-palindrome (the-list)
  (equal the-list (reverse-list the-list)))

(defun reverse-list (the-list)
  (if the-list
    (append (reverse-list (cdr the-list)) (list (car the-list)))
      nil))
